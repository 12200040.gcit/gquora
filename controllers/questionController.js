const Question = require('./../models/queryModel')

const questionController = {

  getAllQuestions: async (req, res) => {
    try {
      const questions = await Question.find();
      res.json({ success: true, questions });
    } catch (error) {
      console.error('Error:', error);
      res.status(500).json({ success: false, error: 'Internal Server Error' });
    }
},

createQuery: async (req, res) => {
    try {
        // Extract the data from the request body, including the "description" field
        const { question, /* other fields */ } = req.body;

        // Perform validation if needed
        if (!question) {
            return res.status(400).json({ error: 'Description is required' });
        }

        // Create a new query instance
        const newQuestion = new Question({
            question,
            // Other fields as needed
        });

        // Save the query to the database
        const savedQuestion = await newQuestion.save();

        res.status(201).json({
            data: savedQuestion,
            status: 'success'
        });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
}

}


module.exports = questionController
