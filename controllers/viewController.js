const path = require('path')

// Login Page
exports.getLoginForm = (req, res) =>{
    res.sendFile(path.join(__dirname,'../','views','login.html'))
}

// Signup Page
exports.getSignupForm = (req, res) =>{
    res.sendFile(path.join(__dirname,'../','views','signup.html'))
}

// Home Page
exports.getHome = (req, res) =>{
    res.sendFile(path.join(__dirname,'../','views','dashboard.html'))
}