var Newitem = require('../models/postModel');
const fs = require('fs');
const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      const uploadDir = 'public/uploads/';
  
      // Create the directory if it doesn't exist
      if (!fs.existsSync(uploadDir)) {
        fs.mkdirSync(uploadDir, { recursive: true });
      }
  
      cb(null, uploadDir);
    },
    filename: (req, file, cb) => {
      cb(null, file.originalname);
    },
  });
  
  const upload = multer({ storage });

  
  exports.createItem = async (req, res) => {
    upload.single('image')(req, res, async (err) => {
      if (err) {
        return res.status(400).json({ error: 'Error uploading the image' });
      }
  
      const { title, description } = req.body;
  
      try {
        // Create a new data entry
        const newData = new Newitem({
          title,
          description,
          image: `http://localhost:4001/uploads/${req.file.filename}`, // URL to the image
        });
  
        await newData.save();
        res.redirect('/dashboard.html');
      } catch (err) {
        res.status(500).json({ error: 'Server error' });
      }
    });
  };
  
  
  exports.updateItem = async (req, res) => {
    const dataId = req.params.id;
    const updatedData = req.body;
  
    try {
        // Find the data by ID
        const data = await Newitem.findById(dataId);
  
        if (!data) {
            return res.status(404).json({ error: 'Data entry not found' });
        }
  
        // Update the fields you want to change
        data.title = updatedData.title;
        data.description = updatedData.description;
  
        if (req.file) {
          // Remove the old image file from your server (if needed)
          // Update the reference to the image URL with the new file name
          data.image = `http://localhost:4001/uploads/${req.file.filename}`;
        }
  
        // Save the updated data
        await data.save();
       
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'An error occurred while updating data' });
    }
  };
  
  
  // Get all data entries
  exports.getAllItems = async (req, res) => {
    try {
      const data = await Newitem.find();
      res.json(data);
    } catch (error) {
      res.status(500).json({ error: 'An error occurred while fetching data' });
    }
  };
  
  // Delete a data entry by ID
  exports.deleteItem = async (req, res) => {
    const dataId = req.params.id; // Get the data entry ID from the request parameters
  
    try {
      // Find the data entry by ID and remove it
      const data = await Newitem.findByIdAndRemove(dataId);
  
      if (!data) {
        return res.status(404).json({ error: 'Data entry not found' });
      }
  
      res.json(data);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'An error occurred while deleting data' });
    }
  };

    exports.counttotalItem = async (req, res) => {
    try {
      const count = await Newitem.countDocuments(); // Count the documents in the User collection
      res.json({ count });
    } catch (error) {
      res.status(500).json({ error: 'Could not get user count' });
    }
  };

  // // Get the total count of items
  // exports.getItemCount = (req, res) => {
  //   Newitem.countDocuments({}, (err, count) => {
  //     if (err) {
  //       return res.status(500).json({ error: err });
  //     }
  //     res.status(200).json({ count });
  //   });
  // };

  // Get the total count of items
  exports.getItemCount = async (req, res) => {
    try {
      const count = await Newitem.countDocuments({});
      res.status(200).json({ count });
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  };
  
  
  
  
  










































// // const path = require('path');
// exports.getAllItems = async(req, res, next) => {S
//     try{
//         const newitems = await Newitem.find()
//         res.status(200).json({data:newitems, status:'success'})
//     } catch(err){
//         res.status(500).json({error: err.message});
//     }
// }

// exports.createItem = async (req, res) => {
//     try{
//         const newitem = await Newitem.create(req.body);
//         console.log(req.body.name)
//         res.json({data:newitem, status :"success"});
//     }catch(err){
//         res.status(500).json({error:err.message});
//     }
// }

// exports.getItem = async (req, res) => {
//     try{
//         const newitem = await Newitem.findById(req.params.id);
//         res.json({data:newitem, status :"success"});
//     }catch(err){
//         res.status(500).json({error:err.message});
//     }
// }

// exports.updateItem = async (req, res) => {
//     try{
//         const item = await Newitem.findByIdAndUpdate(req.params.id, req.body);
//         res.json({data:item, status :"success"});
//     }catch(err){
//         res.status(500).json({error:err.message});
//     }
// }

// exports.deleteItem = async (req, res) => {
//     try{
//         const item = await Newitem.findByIdAndDelete(req.params.id);
//         res.json({data:item, status :"success"});
//     }catch(err){
//         res.status(500).json({error:err.message});
//     }
// }
