// userPostController.js
const UserPost = require('../models/userpostModel');
const fs = require('fs');
const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const uploadDir = 'public/uploads/';

        if (!fs.existsSync(uploadDir)) {
            fs.mkdirSync(uploadDir, { recursive: true });
        }

        cb(null, uploadDir);
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    },
});

const upload = multer({ storage });

// Create a new user post
exports.createUserPost = async (req, res) => {
    upload.single('image')(req, res, async (err) => {
        if (err) {
            return res.status(400).json({ error: 'Error uploading the image' });
        }

        const { title, description } = req.body;

        try {
            const newData = new UserPost({
                title,
                description,
                image: `http://localhost:4001/uploads/${req.file.filename}`,
            });

            await newData.save();
            res.redirect('/dashboard.html');
        } catch (err) {
            res.status(500).json({ error: 'Server error' });
        }
    });
};

// Update a user post
exports.updateUserPost = async (req, res) => {
    const postId = req.params.id;
    const updatedData = req.body;

    try {
        const data = await UserPost.findById(postId);

        if (!data) {
            return res.status(404).json({ error: 'User post not found' });
        }

        data.title = updatedData.title;
        data.description = updatedData.description;

        if (req.file) {
            data.image = `http://localhost:4001/uploads/${req.file.filename}`;
        }

        await data.save();
        res.status(200).json({ message: 'User post updated successfully' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'An error occurred while updating user post' });
    }
};

// Get all user posts
exports.getAllUserPosts = async (req, res) => {
    try {
        const data = await UserPost.find();
        res.json(data);
    } catch (error) {
        res.status(500).json({ error: 'An error occurred while fetching user posts' });
    }
};

// Delete a user post by ID
exports.deleteUserPost = async (req, res) => {
    const postId = req.params.id;

    try {
        const data = await UserPost.findByIdAndRemove(postId);

        if (!data) {
            return res.status(404).json({ error: 'User post not found' });
        }

        res.json({ message: 'User post deleted successfully' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'An error occurred while deleting user post' });
    }
};

// Get the total count of user posts
exports.getUserPostCount = async (req, res) => {
    try {
        const count = await UserPost.countDocuments({});
        res.status(200).json({ count });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};
