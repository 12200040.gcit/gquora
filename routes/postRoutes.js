const express = require('express')
const newitemController = require('../controllers/postController') 
const router = express.Router()

const multer = require('multer');
const fs = require('fs');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const uploadDir = 'public/uploads';

    // Create the directory if it doesn't exist
    if (!fs.existsSync(uploadDir)) {
      fs.mkdirSync(uploadDir, { recursive: true });
    }

    cb(null, uploadDir);
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });


router.post('/item',newitemController.createItem)
router.get('/get-item',newitemController.getAllItems)
router.get('/get-count', newitemController.getItemCount);
router.put('/update-item/:id',upload.single('image'),newitemController.updateItem);
router.delete('/deleteitem/:id', newitemController.deleteItem);

// Count Total Item
router.delete('/get-count', newitemController.counttotalItem );

module.exports = router
