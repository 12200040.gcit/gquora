// userPostRoutes.js
const express = require('express');
const userPostController = require('../controllers/userPostController');
const router = express.Router();

const multer = require('multer');
const fs = require('fs');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const uploadDir = 'public/uploads';

        if (!fs.existsSync(uploadDir)) {
            fs.mkdirSync(uploadDir, { recursive: true });
        }

        cb(null, uploadDir);
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    },
});

const upload = multer({ storage });

router.post('/userpost', userPostController.createUserPost);
router.get('/get-userposts', userPostController.getAllUserPosts);
router.put('/update-userpost/:id', upload.single('image'), userPostController.updateUserPost);
router.delete('/deleteuserpost/:id', userPostController.deleteUserPost);
router.get('/get-userpost-count', userPostController.getUserPostCount);

module.exports = router;
