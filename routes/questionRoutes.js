const express = require('express')
const questionController = require('./../controllers/questionController')

const router = express.Router()

router.post('/questions',questionController.createQuery)
router.get('/questions',questionController.getAllQuestions)
module.exports = router;

// router.post('/login', authController.login)
