const express = require('express')
const router = express.Router()
const viewsController = require('./../controllers/viewController')
const authController = require("./../controllers/authController")

router.get('/', viewsController.getLoginForm)
router.get('/dashboard', viewsController.getHome)
router.get('/signup', viewsController.getSignupForm)
router.get("/logout", authController.logout);
// router.get("/resources", viewsController.get)


module.exports = router