const express = require("express")
const path = require('path')
const app = express()
const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')
const questionRouter = require('./routes/questionRoutes')
const postRouter = require('./routes/postRoutes')
const userPostRouter = require('./routes/userPostRoutes')

app.use(express.json())
app.use('/api/v1/users',userRouter)
app.use('/',viewRouter)
app.use('/api/v1/questions',questionRouter)
app.use('/api/v1/item', postRouter)
app.use('/api/v1/userpost',userPostRouter)


app.use(express.static('public'));
app.use('/public', express.static('public'));


app.use(express.static(path.join(__dirname, 'views')))
module.exports = app

