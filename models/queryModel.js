const mongoose = require('mongoose')

const querySchema = new mongoose.Schema({
    question:{
        type: String,
        required: [true, 'write your question']
    }
})

const Question = mongoose.model('Question',querySchema)
module.exports = Question
