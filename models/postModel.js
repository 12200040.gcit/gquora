const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    title : {
        type : String,
        required: true
    },
    description : {
        type: String,
        required: true,
    },
    image : {
        type: String,
        required: true,
    }
})
const Newitem = mongoose.model('Post', schema);

module.exports = Newitem;

