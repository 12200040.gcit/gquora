import { showAlert } from "./alert.js";

async function fetchUsers() {
  try {
    const response = await axios.get("http://localhost:4001/api/v1/users"); // Use the correct endpoint
    const users = response.data.data; // Access the 'data' property

    // Get a reference to the table's <tbody> element
    const tableBody = document.querySelector("#user-table tbody");

    // Loop through the retrieved user data and create table rows
    users.forEach((user, index) => {
      const row = document.createElement("tr");

      // Create and populate the cells
      const slNoCell = document.createElement("td");
      slNoCell.textContent = index + 1;
      row.appendChild(slNoCell);

      const emailCell = document.createElement("td");
      emailCell.textContent = user.email; // Use the correct field from your user data
      row.appendChild(emailCell);

      const actionCell = document.createElement("td");
      const deleteButton = document.createElement("button");
      deleteButton.classList.add("btn", "delete-user-button");
      deleteButton.setAttribute("data-user-id", user._id);
      deleteButton.textContent = "Delete";

      // Add an event listener for the delete button
      deleteButton.addEventListener("click", async () => {
        const confirmDelete = confirm(
          "Are you sure you want to delete this user?"
        );
        if (confirmDelete) {
          try {
            const deleteResponse = await axios.delete(
              `http://localhost:4001/api/v1/users/${user._id}`
            );

            if (deleteResponse.data.status === "success") {
              tableBody.removeChild(row);
              alert("Success: User deleted");
            } else {
              alert("Error: User deletion failed");
            }
          } catch (deleteError) {
            alert("Error: User deletion failed");
            console.error("Error:", deleteError);
          }
        }
      });

      actionCell.appendChild(deleteButton); // Add the delete button
      row.appendChild(actionCell);
      tableBody.appendChild(row);
    });
  } catch (error) {
    console.error(error);
    // Handle errors as needed
  }
}

// Call the fetchUsers function to populate the table when the page loads
fetchUsers();
