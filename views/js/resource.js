// import axios from 'axios';
import { resource } from "../../app.js"
import { showAlert } from "./alert.js"


export const addresources = async (resource) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:4001/api/v1/resources/resources',
            data: {
                resource
            },
        })
        if (res.data.status === 'success') {
            showAlert('success', 'Resource successfully added')
            // window.setTimeout(() => {
            //     location.assign('/login')
            // }, 1500)
        }
    } catch(err) {
        console.log("required")
        let message =
            typeof err.response !== 'undefined'
                ? err.response.data.message
                : err.message
        showAlert('error', 'Error: resources required', message)
    }
}

document.querySelector('.form').addEventListener('submit', (e) => {
    e.preventDefault()
    const resource = document.getElementById('description').value
    addresources(resource)
})
