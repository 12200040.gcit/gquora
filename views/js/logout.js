// logout.js

const logout = async () => {
    try {
      const res = await axios({
        method: "GET",
        url: "http://localhost:4001/login",
      });
      console.log(res);
      if (res.data.status === "success") {
        location.reload(true);
      }
    } catch (err) {
      showAlert("error", "Error logging out! Try again.");
    }
  };