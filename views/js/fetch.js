import { showAlert } from "./alert.js";
document.addEventListener("DOMContentLoaded", async function () {
    try {
        console.log('try')
        const response = await fetch("http://localhost:4001/api/v1/questions/questions", {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        });

        const data = await response.json();
        console.log(data)
        if (data) {
            console.log('working')
            displayQuestions(data.questions);
        } else {
            showAlert("error", "Failed to fetch questions");
        }
    } catch (error) {
        showAlert("error", "An error occurred while fetching questions");
        console.error("Error:", error);
    }
});

function displayQuestions(questions) {
    const questionContainer = document.getElementById("questionContainer");

    questions.forEach((question) => {
        const questionCard = createQuestionCard(question);
        questionContainer.appendChild(questionCard);
    });
}

function createQuestionCard(question) {
    const card = document.createElement("div");
    card.classList.add("mb-4");

    card.innerHTML = `
    <div class="simple-question">
    <h6 class="">${question.question}</h6>
    </div>
    <hr>

    `;

    return card;
}