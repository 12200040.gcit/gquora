import { showAlert } from "./alert.js"
//LOgging out

// export const logout = async () => {
//     try {
//       const res = await axios({
//         method: "GET",
//         url: "http://localhost:4001/api/v1/Admin/logoutA",
//       });
//       console.log(res);
//       if (res.data.status === "success") {
//         location.reload(true);
//       }
//     } catch (err) {
//       showAlert("error", "Error logging out! Try again.");
//     }
//   };

  
// for post question
export const addquestions = async (question) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:4001/api/v1/questions/questions',
            data: {
                question
            },
        })
        if (res.data.status === 'success') {
            showAlert('success', 'Question successfully added')
            // window.setTimeout(() => {
            //     location.assign('/login')
            // }, 1500)
        }
    } catch(err) {
        console.log("required")
        let message =
            typeof err.response !== 'undefined'
                ? err.response.data.message
                : err.message
        showAlert('error', 'Error: question required', message)
    }
}

document.querySelector('.form').addEventListener('submit', (e) => {
    e.preventDefault()
    const question = document.getElementById('question').value
    addquestions(question)
})
