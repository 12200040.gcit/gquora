import { showAlert } from './alert.js';

const createPost = async (title, description, image) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:4001/api/v1/posts', // Replace with your API endpoint
            data: {
                title,
                description,
                //image,
            },
        });
        if (res.data.status === 'success') {
            showAlert('success', 'Post created successfully');
            // Optionally, you can redirect or perform other actions here
        }
    } catch (err) {
        let message =
            typeof err.response !== 'undefined'
                ? err.response.data.message
                : err.message;
        showAlert('error', `Failed to create the post. ${message}`);
    }
};

// Assuming you have an HTML form for creating posts:
document.querySelector('.post-form').addEventListener('submit', async (e) => {
    e.preventDefault();
    const title = document.getElementById('title').value;
    const description = document.getElementById('description').value;
   // const image = document.getElementById('image').value; // You may want to handle file uploads differently
    await createPost(title, description)//, image);
});
