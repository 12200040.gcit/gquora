import { showAlert } from "./alert.js";
var obj = JSON.parse(document.cookie.substring(6));

var el = document.querySelector(".form.form-user-data");
el.innerHTML =
  `<button class="btn btn-view-issues">View Issues</button> <div class = 'form__group' ><label class="form__label" for="email">Email address</label>
<input class="form__input" id="email" type="email" value="` +
  obj.email +
  `" required="required" name="email" disabled/></div>`;

var el2 = document.querySelector("form.form-user-password");
el2.innerHTML = `   <div class="form__group">
                        <label class="form__label" for="password-current">Current password</label>
                        <input class="form__input" id="password-current" type="password" placeholder="••••••••"
                            required="required" minlength="8" />
                    </div>
                    <div class="form__group">
                        <label class="form__label" for="password">New password</label>
                        <input class="form__input" id="password" type="password" placeholder="••••••••"
                            required="required" minlength="8" />
                    </div>
                    <div class="form__group ma-bt-lg">
                        <label class="form__label" for="password-confirm">Confirm password</label>
                        <input class="form__input" id="password-confirm" type="password" placeholder="••••••••"
                            required="required" minlength="8" />
                    </div>
                    <div class="form__group right">
                    <button class="btn btn-profile">Save password</button>
                    </div>`;
//updating setting

// type is either 'password' or data
// In the updateSettings function
export const updateSettings = async (data, type) => {
  try {
    const url =
      type === "password"
        ? "http://localhost:4001/api/v1/users/updateMyPassword"
        : "http://localhost:4001/api/v1/users/updateMe";
    const res = await axios({
      method: "PATCH",
      url,
      data,
    });
    console.log(res.data.status);
    if (res.data.status === "success") {
      showAlert("success", "Data updated successfully!");
    }
  } catch (err) {
    if (err.response && err.response.data.error) {
      showAlert("error", err.response.data.error);
    } else {
      showAlert("error", "An error occurred. Please try again later.");
    }
  }
};

const userDataForm = document.querySelector(".form.form-user-data");
userDataForm.addEventListener("submit", (e) => {
  e.preventDefault();
  var obj = JSON.parse(document.cookie.substring(6));
  const form = new FormData();
  form.append("email", document.getElementById("email").value);
  form.append("userId", obj._id);
  console.log(form);
  updateSettings(form, "data");
});
const userPasswordForm = document.querySelector("form.form-user-password");
userPasswordForm.addEventListener("submit", async (e) => {
  e.preventDefault();

  document.querySelector(".btn-profile").textContent = "Updating...";
  const passwordCurrent = document.getElementById("password-current").value;
  const password = document.getElementById("password").value;
  const passwordConfirm = document.getElementById("password-confirm").value;
  await updateSettings(
    { passwordCurrent, password, passwordConfirm },
    "password"
  );
  document.querySelector(".btn-profile").textContent = "Save password";
  document.getElementById("password-current").value = "";
  document.getElementById("password").value = "";
  document.getElementById("password-confirm").value = "";
});

const viewIssuesButton = document.querySelector(".btn-view-issues");
viewIssuesButton.addEventListener("click", () => {
  // Navigate to the "viewIssues" page with the user ID
  window.location.href = `/viewIssues/${obj._id}`;
});
